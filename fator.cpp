#include <iostream>
#include <iomanip>

float verifica(int num, float divisor, int &resultado){
    float verificador = 0.00;
    resultado = num/divisor;
    verificador = num/divisor;
    float numero = float(resultado);
    if (numero == verificador){
        return divisor;
    }
    return verifica(num, ++divisor, resultado);
}

void fatorprimo(int num){
    float divisor = 2;
    int resultado = 0;
    if (num > 1){
        divisor = verifica(num, divisor, resultado);
        std::cout << divisor << " " << std::flush;
        fatorprimo(resultado);
    }
}

int main(int argc, char const *argv[]) {
    int x = 0;
    std::cout << "|" << std::setw(16) << std::setfill('#') << "|" << std::endl << std::flush;
    std::cout << "|" << "  Fator Primo  " << "|\n" << std::flush;
    std::cout << "|" << std::setw(16) << std::setfill('#') << "|" << std::endl << std::endl << std::flush;
    std::cout << "Digite um número maior que 1: " << std::flush;
    std::cin >> x;
    if (x > 1){
        std::cout << "\nO Fator primo de " << x << " é: " << std::flush;
        fatorprimo(x);
    }
    std::cout << "\n\n\n###### FIM DO PROGRAMA ######\n\n\n";
    return 0;
}
