// Author: Submit Equals

#include <iostream>
#include <iomanip>
#include <chrono>
#include <thread>

int opcao(unsigned short int &operacao){
	short int num = 0;
	std::cout << "Escolha uma opção: " << std::flush;
	std::cin >> operacao;
	std::cin.ignore();
	system("clear");
	if (operacao != 0 && operacao <= 3 && operacao > 0){
		std::cout << "Digite o número que deseja converter: " << std::flush;
		std::cin >> num;
		system("clear");
	}
	switch (operacao){
		case 0:
			exit(1);
			break;
		case 1:
			std::cout << "A conversão para Hexadecimal é: " 
					  << std::hex << num << std::flush;
		    std::this_thread::sleep_for(std::chrono::milliseconds(3000));
			break;
		case 2:
			std::cout << "A conversão para Decimal é: " 
					  << std::dec << num << std::flush;
			std::this_thread::sleep_for(std::chrono::milliseconds(3000));
			break;
		case 3:
			std::cout << "A conversão para Octal é: " 
					  << std::oct << num << std::flush;
			std::this_thread::sleep_for(std::chrono::milliseconds(3000));
			break;
		default:
			std::cout << "Digite um número de 0 até 3." << std::flush;
			std::this_thread::sleep_for(std::chrono::milliseconds(3000));
			break;
	return 0;
	}
}

int menu(){
	unsigned short int op = 0;
	std::cout << "+----Conversão---+"
			  << std::endl << "|" 
			  << std::left 
			  << std::setw(16) << std::setfill(' ') << "0 - Sair"
			  << "|\n"
			  << "|"
			  << std::left
			  << std::setw(16) << std::setfill(' ') << "1 - Hexadecimal"
			  << "|\n" 
			  << "|"
			  << std::left
			  << std::setw(16) << std::setfill(' ') << "2 - Decimal"
			  << "|\n"
			  << "|"
			  << std::left
			  << std::setw(16) << std::setfill(' ') << "3 - Octal"
			  << "|\n"
			  << "+" << std::setw(18) << std::setfill('-') 
			  << std::right << "+\n"
			  << std::endl << std::flush;
	opcao(op);
	return 0;
}


int main(){
	while (true){
		system("clear");
		menu();
	}
	return 0;
}